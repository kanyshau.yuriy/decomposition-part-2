package com.ehu.decomposition;

import java.util.ArrayList;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
    }

    public static int findGreatestCommonDivisor(final int firstNumber, final int secondNumber) {
        if (secondNumber == 0) {
            return firstNumber;
        }
        return findGreatestCommonDivisor(secondNumber, firstNumber % secondNumber);
    }
    public static boolean checkAreCoprimeNumbers(final int a, final int b, final int c) {
        int firstResult  = findGreatestCommonDivisor(a, b);
        int secondResult = findGreatestCommonDivisor(a, c);
        int threeResult = findGreatestCommonDivisor(b, c);
        return firstResult == secondResult && firstResult == threeResult;
    }

    public static ArrayList<Integer> armstrongNumbers(final int to) {

        ArrayList<Integer> armstrongNumbers = new ArrayList<>();

        int i = 0;
        while (i < to) {
            int[] digits = splitNumberToDigits(i);
            int sumOfDigits = 0;

            for(int digit : digits) {
                sumOfDigits += Math.pow(digit, 3);
            }
            if (sumOfDigits == i)
                armstrongNumbers.add(i);

            i++;
        }
        return armstrongNumbers;
    }

    public static int[] splitNumberToDigits(int number) {
        int length = String.valueOf(number).length();
        int[] result = new int[length];

        while (number != 0) {
            result[--length] = number % 10;
            number /= 10;
        }
        return result;
    }

    public static int calculateSumOfArrayElements(final int[] numbers, final int from, final int to) {
        int lastElementKey = numbers.length - 1;

        if (to > lastElementKey || from > lastElementKey)
            return 0;

        int sum = 0;
        for (int i = from; i < to; i++) {
            sum += numbers[i];
        }
        return sum;
    }
}

